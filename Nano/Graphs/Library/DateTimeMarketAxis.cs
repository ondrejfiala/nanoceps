﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OxyPlot;

namespace Graphs.Library

{
    /// <summary>
    /// Osa s časovým měřítkem pro trhy (např. H12)
    /// - WPF obal
    /// </summary>
    public class DateTimeMarketAxis : OxyPlot.Wpf.DateTimeAxis
    {
        /// <summary>
        /// Konstruktor osy
        /// </summary>
        public DateTimeMarketAxis()
        {
            //base();
            this.InternalAxis = new NewInternalDateTimeAxis();
        }
    }

    /// <summary>
    /// Interní osa s vlastnostmi
    /// </summary>
    public class NewInternalDateTimeAxis : OxyPlot.Axes.DateTimeAxis
    {
        /// <summary>
        /// Konstruktor
        /// </summary>
        public NewInternalDateTimeAxis()
        {

        }

        /// <summary>
        /// Formát popisek osy
        /// </summary>
        /// <param name="x">Umístění bodu</param>
        /// <returns>Řetězec ve formátu H12</returns>
        public new string FormatValue(double x)
        {
            if (this.StringFormat.Equals("HH"))
            {
                string orig = base.FormatValue(x);
                if (orig.Equals("00"))
                    orig = "24";
                return "H" + orig;
            } else
            {
                return base.FormatValue(x);
            }
        }

        /// <summary>
        /// Nastavení jemnosti osy
        /// </summary>
        /// <param name="majorLabelValues">Popis hlavních intervalů</param>
        /// <param name="majorTickValues">Délka hlavních itervalu</param>
        /// <param name="minorTickValues">Délka vedlejších intervalů</param>
        public override void GetTickValues(out IList<double> majorLabelValues, out IList<double> majorTickValues, out IList<double> minorTickValues)
        {
            base.GetTickValues(out majorLabelValues, out majorTickValues, out minorTickValues);
            // pri nekonecnu vraci 1000, ci 601 popisek - oprava
            if (majorLabelValues.Count >= 500)
            {
                majorLabelValues = new List<double> { 0.0 };
                majorTickValues = new List<double> { 0.0 };
                minorTickValues = new List<double> { 0.0 };
            }
            else
            {
                // zaokrouhlit na cele hodiny
                double hour = 1.0 / 24;
                for(int i = 0; i < majorLabelValues.Count; i++)
                {
                    double part = (majorLabelValues[i] % 1);
                    // o hodinu vic
                    int current = Convert.ToInt32(part / hour);
                    majorLabelValues[i] = Math.Truncate(majorLabelValues[i]) + current * hour;
                }
            }
        }
    }
}
